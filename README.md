# Yew Webacpk

## Prerequisites

[rust](https://www.rust-lang.org/)
[wasm-pack](https://rustwasm.github.io/)

```sh
sudo apt install build-essential
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```

## 🚴 Usage

### 🛠️ Build

When building for the first time, ensure to install dependencies first.

#### Rust

```sh
wasm-pack build --target web
```

#### Webpack

```sh
yarn install
yarn run build
```

### 🔬 Serve locally

```sh
yarn run start:dev
```
